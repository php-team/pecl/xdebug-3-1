Source: xdebug-3-1
Section: php
Priority: optional
Maintainer: Debian PHP PECL Maintainers <team+php-pecl@tracker.debian.org>
Uploaders: Martin Meredith <mez@debian.org>,
           Lior Kaplan <kaplan@debian.org>,
           Ondřej Surý <ondrej@debian.org>
Build-Depends: debhelper (>= 10~),
               dh-php (>= 4~),
               php7.2-dev,
               php7.3-dev,
               php7.4-dev
Standards-Version: 4.5.1
Homepage: https://xdebug.org/
Vcs-Browser: https://salsa.debian.org/php-team/pecl/xdebug
Vcs-Git: https://salsa.debian.org/php-team/pecl/xdebug.git
X-PHP-Dummy-Package: no
X-PHP-Default-Version: 7.4
X-PHP-Versions: 7.2 7.3 7.4
X-PHP-PECL-Name: xdebug

Package: php7.4-xdebug
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Breaks: ${pecl:Breaks}
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: ${pecl:Provides},
          ${php:Provides}
Replaces: ${pecl:Replaces}
Suggests: ${pecl:Suggests}
Description: Xdebug Module for PHP
 The Xdebug extension helps you debugging your script by providing a lot of
 valuable debug information. The debug information that Xdebug can provide
 includes the following:
 .
    * stack traces and function traces in error messages with:
       - full parameter display for user defined functions
       - function name, file name and line indications
       - support for member functions
    * memory allocation
    * protection for infinite recursions
 .
 Xdebug also provides:
 .
    * profiling information for PHP scripts
    * script execution analysis
    * capabilities to debug your scripts interactively with a debug client

Package: php7.3-xdebug
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Breaks: ${pecl:Breaks}
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: ${pecl:Provides},
          ${php:Provides}
Replaces: ${pecl:Replaces}
Suggests: ${pecl:Suggests}
Description: Xdebug Module for PHP
 The Xdebug extension helps you debugging your script by providing a lot of
 valuable debug information. The debug information that Xdebug can provide
 includes the following:
 .
    * stack traces and function traces in error messages with:
       - full parameter display for user defined functions
       - function name, file name and line indications
       - support for member functions
    * memory allocation
    * protection for infinite recursions
 .
 Xdebug also provides:
 .
    * profiling information for PHP scripts
    * script execution analysis
    * capabilities to debug your scripts interactively with a debug client

Package: php7.2-xdebug
Architecture: any
Pre-Depends: php-common (>= 2:69~)
Breaks: ${pecl:Breaks}
Depends: ucf,
         ${misc:Depends},
         ${pecl:Depends},
         ${php:Depends},
         ${shlibs:Depends}
Provides: ${pecl:Provides},
          ${php:Provides}
Replaces: ${pecl:Replaces}
Suggests: ${pecl:Suggests}
Description: Xdebug Module for PHP
 The Xdebug extension helps you debugging your script by providing a lot of
 valuable debug information. The debug information that Xdebug can provide
 includes the following:
 .
    * stack traces and function traces in error messages with:
       - full parameter display for user defined functions
       - function name, file name and line indications
       - support for member functions
    * memory allocation
    * protection for infinite recursions
 .
 Xdebug also provides:
 .
    * profiling information for PHP scripts
    * script execution analysis
    * capabilities to debug your scripts interactively with a debug client

